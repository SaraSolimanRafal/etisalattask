package com.company.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.company.dao.DepartmentRepository;
import com.company.dao.EmployeeRepository;
import com.company.domain.Department;
import com.company.domain.Employee;

@Service
public class EmployeeServices {
	
	@Autowired
    private EmployeeRepository employeeRepository;
 
    @Autowired
    private DepartmentRepository departmentRepository;
    // Add employee
    public Employee addEmployee(Employee employee) {
    	 
        Department dept = departmentRepository.findById(employee.getDept().getId()).orElse(null);
        if (null == dept) {
            dept = new Department();
        }
        dept.setDepartmentName(employee.getDept().getDepartmentName());
        employee.setDept(dept);
        return employeeRepository.save(employee);
    }
    
    
    //Get all employees
    public List<Employee> getAllEmployees() {
    	 
        return employeeRepository.findAll();
    }

}
