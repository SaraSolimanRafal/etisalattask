package com.company.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.company.domain.Employee;
import com.company.services.EmployeeServices;

@RestController
@RequestMapping("/employeeservices")
public class EmployeeController {
	
	@Autowired
    private EmployeeServices employeeService;
	
	
	@PostMapping("/addemployee")
    public ResponseEntity<Employee> saveEmployee(@RequestBody Employee employee) {
 
        Employee emp = employeeService.addEmployee(employee);
        return new ResponseEntity<>(emp, HttpStatus.OK);
    }
	
	
	
	@GetMapping("/getemployees")
    public ResponseEntity<List<Employee>> getAllEmployees() {
 
        List<Employee> employees = employeeService.getAllEmployees();
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

}
