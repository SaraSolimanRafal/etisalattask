package com.company.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;


@Entity
@Table(name = "department", catalog = "companydb")
public class Department {
	private Integer id;
	private String departmentName;
	private Integer mangerId;
	private Integer numOfEmp;
	private List<Employee> employees;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "dept_name", nullable = false)
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	
	@Column(name = "manaager_id", nullable = false)
	public Integer getMangerId() {
		return mangerId;
	}
	public void setMangerId(Integer mangerId) {
		this.mangerId = mangerId;
	}
	
	
	@Column(name = "num_of_employees")
	public Integer getNumOfEmp() {
		return numOfEmp;
	}
	public void setNumOfEmp(Integer numOfEmp) {
		this.numOfEmp = numOfEmp;
	}
	
	@JsonManagedReference(value = "dept-deptEmployee")
	@OneToMany(mappedBy = "dept", targetEntity = Employee.class, cascade = CascadeType.REMOVE)
	public List<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

}
