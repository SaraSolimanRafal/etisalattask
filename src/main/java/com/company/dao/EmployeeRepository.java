package com.company.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.company.domain.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

}
