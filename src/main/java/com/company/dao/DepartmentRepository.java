package com.company.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.company.domain.Department;

public interface DepartmentRepository extends JpaRepository<Department, Integer>{

}
